
let stepInput = document.getElementById('steps');
let startInput = document.getElementById('start');
let endInput = document.getElementById('end');
let goalInput = document.getElementById('goal');
let proj = document.getElementById('proj');

let stepList = document.getElementById('stepList');

let steps = new Array(0);

function saveList() {
    if (proj.value == "") { return; }
    window.localStorage.setItem(proj.value, JSON.stringify(steps));
    window.localStorage.setItem(proj.value + "_start", JSON.stringify(inputOr(startInput, 1)));
    console.log("saved");
    makeLoadButtons();
}

function autosave() {
    var t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;  // catches touchscreen presses as well
    window.ontouchstart = resetTimer; // catches touchscreen swipes as well
    window.onclick = resetTimer;      // catches touchpad clicks as well
    window.onkeypress = resetTimer;
    window.addEventListener('scroll', resetTimer, true); // improved; see comments

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(saveList, 2000);  // time is in milliseconds
    }
}
autosave();

document.getElementById('saveBtn').onclick = saveList;

let loadBtns = document.getElementById('loadBtns');

function makeLoadButtons() {
    loadBtns.innerHTML = "";
    for (var i = 0; i < window.localStorage.length; i++) {
        if (localStorage.key(i).includes("_start")) { continue; }
        var b = document.createElement("button");
        let key = localStorage.key(i);
        b.innerHTML = key;
        b.onclick = function() {
            if (proj.value != "") { saveList(); }
            proj.value = key;
            loadList();
        };
        loadBtns.appendChild(b);
    }
}

makeLoadButtons();

function inputOr(i, o) {
    return i.value == "" ? 0 : parseInt(i.value);
}

function getOffset() {
    return inputOr(startInput, 1);
}

function loadList() {
    steps = JSON.parse(window.localStorage.getItem(proj.value));
    stepInput.value = steps.length;
    startInput.value = JSON.parse(window.localStorage.getItem(proj.value + "_start"));
    syncCounts("step")();
    stepList.innerHTML = "";
    for (var i = 0; i < steps.length; i++) {
        var li = document.createElement("li");
        var toggle = document.createElement("span");
        toggle.innerHTML = i + 1;
        li.onclick = toggledone(toggle);
        li.oncontextmenu = togglemark(toggle);
        li.appendChild(toggle);
        stepList.appendChild(li);
        if (steps[i].mark) {toggle.classList.toggle("marked")};
        if (steps[i].done) {toggle.classList.toggle("done")};
    }
    updateStart();
}

document.getElementById('delBtn').onclick = function() {
    window.localStorage.removeItem(proj.value);
    proj.value = "";
    makeLoadButtons();
};

function togglemark(el) {
    return function(){
        el.classList.toggle("marked");
        let idx = Array.from(stepList.children).indexOf(el.parentNode);
        steps[idx].mark = !steps[idx].mark;
        return false;
    };
}

function toggledone(el) {
    return function(){
        el.classList.toggle("done")
        let idx = Array.from(stepList.children).indexOf(el.parentNode);
        steps[idx].done = !steps[idx].done;
    };
}

function genStepList() {
    stepList.innerHTML = "";
    for (var i = 1; i <= stepInput.value; i++) {
        var li = document.createElement("li");
        var toggle = document.createElement("span");
        toggle.innerHTML = i;
        li.onclick = toggledone(toggle);
        li.oncontextmenu = togglemark(toggle);
        li.appendChild(toggle);
        steps.push({ done: false,
                     mark: false
                   });
        stepList.appendChild(li);
    }
}

function updateStepList() {
    syncCounts("step")();
    for (var i = steps.length + 1; i <= stepInput.value; i++) {
        var li = document.createElement("li");
        var toggle = document.createElement("span");
        toggle.innerHTML = i;
        li.onclick = toggledone(toggle);
        li.oncontextmenu = togglemark(toggle);
        li.appendChild(toggle);
        steps.push({ done: false,
                     mark: false
                   });
        stepList.appendChild(li);
    }
    while (steps.length > stepInput.value) {
        steps.pop();
        stepList.removeChild(stepList.lastChild);
    }
    updateStart();
}

function updateGoal() {
    let offset = getOffset();
    stepList.querySelectorAll('li>span').forEach((e,i) => { if (i + offset == goalInput.value) {
        e.classList.add("goal");
    } else {
        e.classList.remove("goal");
    }})
}

function updateStart() {
    syncCounts("start")();
    let offset = getOffset();
    stepList.querySelectorAll('li>span').forEach((e,i) => { e.innerHTML = i + offset; })
}

function updateEnd() {
    syncCounts("end")();
    updateStepList();
}

function syncCounts(changed) {
    return function() {
        let startval = getOffset();
        let stepval = inputOr(stepInput, 0);
        let endval = inputOr(endInput, startval);
        switch (changed) {
        case "start":
        case "step":
            endInput.value = startval + stepval - 1;
            break;
        case "end":
            stepInput.value = endval - startval + 1;
            break;
        default:
            console.log("Error, unknown case in syncCounts!");
        }
    };
}

document.getElementById('shareBtn').onclick = function() {
    html2canvas(stepList).then(function(canvas) {canvas.toBlob(function(blob) {
        const item = new ClipboardItem({ "image/png": blob });
        navigator.clipboard.write([item]);
    });})
};

document.getElementById('resetBtn').onclick = function() {
    for (var i = 0; i < steps.length; i++) {
        steps[i].done = false;
        saveList();
        loadList();
    }
};

stepInput.onchange = updateStepList;
startInput.onchange = updateStart;
endInput.onchange = updateEnd;
goalInput.onchange = updateGoal;
genStepList();
